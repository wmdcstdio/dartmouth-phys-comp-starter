
#ifndef DARTMOUTH_PHYS_COMP_VORTEXFLUIDDRIVER_H
#define DARTMOUTH_PHYS_COMP_VORTEXFLUIDDRIVER_H
#include <random>
#include "Common.h"
#include "Driver.h"
#include "OpenGLMarkerObjects.h"
#include "OpenGLCommon.h"
#include "OpenGLWindow.h"
#include "OpenGLViewer.h"
#include "VortexFluid.h"

template<int d> class VortexFluidDriver: public Driver, public OpenGLViewer{
using VectorD=Vector<real,d>;
real dt = 0.2;
VortexFluid<d> fluid;
Array<OpenGLSolidCircle*> opengl_circles;
public:
    real frand(real a, real b){
        real  r = (real)rand() / (real)RAND_MAX;
        return (b-a)*r+a;
    }

    virtual void Initialize(){
        int num_tracer = 10000;
        for(int i = 0; i < num_tracer; i++){
            VectorD pos;
            pos[0] = frand(-2,2);
            pos[1] = frand(-0.5,0.5);
            Add_Particle(pos);
        }
        fluid.Source();
        OpenGLViewer::Initialize();
    }
    virtual void Initialize_Data(){
        for(int i = 0; i < fluid.particles.Size(); i++){
            Add_Solid_Circle(i);
        }
        for(int i = 0; i < fluid.vortexes.size(); i++){
            Add_Solid_Circle4vortex(i);
        }
    }

    void Sync_Simulation_And_Visualization_Data()
    {
        for(int i = 0;i < fluid.particles.Size(); i++){
            auto opengl_circle=opengl_circles[i];
            opengl_circle->pos=V3(fluid.particles.X(i));
            real c=(fluid.particles.C(i))/(real)100;
            opengl_circle->color=OpenGLColor(c,.6f,.2f);
            opengl_circle->Set_Data_Refreshed();
        }
        int p_size = fluid.particles.Size();
        for(int i = 0; i < fluid.vortexes.size(); i++){
            auto opengl_circle=opengl_circles[p_size+i];
            opengl_circle->pos=V3(fluid.vortexes[i].pos);
            opengl_circle->Set_Data_Refreshed();
        }

    }

    virtual void Toggle_Next_Frame(){
        fluid.Advance(dt);
        Sync_Simulation_And_Visualization_Data();
        OpenGLViewer::Toggle_Next_Frame();
    }

    virtual void Run(){
        OpenGLViewer::Run();
    }
protected:
    void Add_Particle(VectorD pos,real m=1.){
        int i=fluid.particles.Add_Element();	////return the last element's index
        fluid.particles.X(i)=pos;
        fluid.particles.V(i)=VectorD::Zero();
        fluid.particles.R(i)=.005;
        fluid.particles.M(i)=m;
        fluid.particles.D(i)=1.;
    }
    void Add_Solid_Circle(const int i){
        OpenGLColor c(0.5f,0.5f,0.5f,1.f);
        auto opengl_circle=Add_Interactive_Object<OpenGLSolidCircle>();
        opengl_circles.push_back(opengl_circle);
        opengl_circle->pos=V3(fluid.particles.X(i));
        opengl_circle->radius=fluid.particles.R(i);
        opengl_circle->color=c;
        opengl_circle->Set_Data_Refreshed();
        opengl_circle->Initialize();
    }

    void Add_Solid_Circle4vortex(const int i){
        OpenGLColor c(1.f,0.f,0.f,1.f);
        auto opengl_circle=Add_Interactive_Object<OpenGLSolidCircle>();
        opengl_circles.push_back(opengl_circle);
        opengl_circle->pos=V3(fluid.vortexes[i].pos);
        opengl_circle->radius=0.2;
        opengl_circle->color=c;
        opengl_circle->Set_Data_Refreshed();
        opengl_circle->Initialize();
    }
    ////Helper function to convert a vector to 3d, for c++ template
    Vector3 V3(const Vector2& v2){return Vector3(v2[0],-v2[1],.0);}
    Vector3 V3(const Vector3& v3){return v3;}
};
#endif //DARTMOUTH_PHYS_COMP_VORTEXFLUIDDRIVER_H
