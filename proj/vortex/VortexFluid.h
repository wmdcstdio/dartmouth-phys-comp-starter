
#ifndef DARTMOUTH_PHYS_COMP_VORTEXFLUID_H
#define DARTMOUTH_PHYS_COMP_VORTEXFLUID_H

#include "Common.h"
#include "Vortex.h"
#include "Particles.h"
#define EPS 0.01
#define PI 3.141592653

template<int d> class VortexFluid{
    using VectorD = Vector<real, d>;
    using Vortex_ = Vortex<d>;
public:
    Array<Vortex_> vortexes;
    Particles<d> particles;

    void Advance(const real dt){
        for (int i = 0; i < particles.Size(); i++){
            runge_kutta4pos_update(particles.X(i), vortexes, dt);
        }

        update_vortexes(vortexes, dt);
    }
    void Source(){
        vortexes.push_back(Vortex_(1.0, 0.0, 2.0));
        vortexes.push_back(Vortex_(-1.0, 0.0, -2.0));
        vortexes.push_back(Vortex_(0.25,  0.0,  0.5));
        vortexes.push_back(Vortex_(-0.25, 0.0, -0.5));
        vortexes.push_back(Vortex_(0.5,  0.0,  1.0));
        vortexes.push_back(Vortex_(-0.5, 0.0, -1.0));
        vortexes.push_back(Vortex_(0.75,  0.0,  1.5));
        vortexes.push_back(Vortex_(-0.75, 0.0, -1.5));
    }
    //u component of velocity from a single vortex
    real u_from_vortex(VectorD pos, Vortex_ &vortex)
    {
        real r_ij2 = (pos - vortex.pos).transpose() * (pos - vortex.pos);
        return vortex.vor * (vortex.pos[1] - pos[1]) / (r_ij2 * PI) * 0.5 * (1.0 - exp(-r_ij2/(EPS*EPS)));
    }

    //v component of velocity from a single vortex
    real v_from_vortex(VectorD pos, Vortex_ &vortex)
    {
        real r_ij2 = (pos - vortex.pos).transpose() * (pos - vortex.pos);
        return vortex.vor * (pos[0] - vortex.pos[0]) / (r_ij2 * PI) * 0.5 * (1.0 - exp(-r_ij2/(EPS*EPS))) ;
    }

    // all vortex influence
    real u_from_all_vortexes(VectorD pos, Array<Vortex_> &vortex)
    {
        real u = 0.;
        for(int i = 0; i < vortex.size(); i++){
            u += u_from_vortex(pos, vortex[i]);
        }
        return u;
    }

    real v_from_all_vortexes(VectorD pos, Array<Vortex_> &vortex){
        real v = 0.;
        for(int i = 0; i < vortex.size(); i++){
            v += v_from_vortex(pos, vortex[i]);
        }
        return v;
    }
    void runge_kutta4pos_update(VectorD& pos, Array<Vortex_> &vortex, real dt){
        real x0 = pos[0], y0 = pos[1];

        real u0 = u_from_all_vortexes(pos, vortex);
        real v0 = v_from_all_vortexes(pos, vortex);
        real x1 = x0 + 0.25*dt*u0; real y1 = y0 + 0.25*dt*v0;

        VectorD tmp_pos(x1, y1);
        real u1 = u_from_all_vortexes(tmp_pos, vortex);
        real v1 = v_from_all_vortexes(tmp_pos, vortex);
        real x2 = x0 + 0.5*dt*u1; real y2 = y0 + 0.5*dt*v1;

        tmp_pos[0] = x2, tmp_pos[1] = y2;
        real u2 = u_from_all_vortexes(tmp_pos, vortex);
        real v2 = v_from_all_vortexes(tmp_pos, vortex);
        real x3 = x0 + 0.75*dt*u2; real y3 = y0 + 0.75*dt*v2;

        tmp_pos[0] = x3, tmp_pos[1] = y3;
        real u3 = u_from_all_vortexes(tmp_pos, vortex);
        real v3 = v_from_all_vortexes(tmp_pos, vortex);

        pos[0] += 0.1666*dt*u0 + 0.333*dt*u1 + 0.333*dt*u2 + 0.1666*dt*u3;
        pos[1] += 0.1666*dt*v0 + 0.333*dt*v1 + 0.333*dt*v2 + 0.1666*dt*v3;
//
//        real u0 = u_from_all_vortexes(pos, vortex);
//        real v0 = v_from_all_vortexes(pos, vortex);
//        real x1 = x0 + 0.5*dt*u0; real y1 = y0 + 0.5*dt*v0;
//
//        VectorD tmp_pos(x1, y1);
//        real u1 = u_from_all_vortexes(tmp_pos, vortex);
//        real v1 = v_from_all_vortexes(tmp_pos, vortex);
//        real x2 = x0 + 0.75*dt*u1; real y2 = y0 + 0.75*dt*v1;
//
//        tmp_pos[0] = x2, tmp_pos[1] = y2;
//        real u2 = u_from_all_vortexes(tmp_pos, vortex);
//        real v2 = v_from_all_vortexes(tmp_pos, vortex);
//
//        pos[0] += 0.222*dt*u0 + 0.333*dt*u1 + 0.444*dt*u2;
//        pos[1] += 0.222*dt*v0 + 0.333*dt*v1 + 0.444*dt*v2;

    }

    void update_vortexes(Array<Vortex_>& vortexs, real dt){
        Array<Vortex_> vortex_tmp;
        vortex_tmp.resize(vortexs.size());
        for(int i = 0; i < vortexs.size(); i++){ // something wrong in the = operator?
            vortex_tmp[i].pos = vortexs[i].pos;
            vortex_tmp[i].vor = vortexs[i].vor;
//            vortex_tmp[i].Initialize(vortexs[i].pos, vortexs[i].vor);
        }
        for(int i = 0; i < vortex_tmp.size(); i++){
            real u = 0., v = 0.;
            for(int j = 0; j < vortex_tmp.size(); j++){
                if (j != i){
                    u += u_from_vortex(vortex_tmp[i].pos, vortexs[j]);
                    v += v_from_vortex(vortex_tmp[i].pos, vortexs[j]);
                }
            }
            vortex_tmp[i].pos[0] += dt * u;
            vortex_tmp[i].pos[1] += dt * v;
        }
        for(int i = 0; i < vortexs.size(); i++){
            vortexs[i].pos = vortex_tmp[i].pos;
            vortexs[i].vor = vortex_tmp[i].vor;
//            vortexs[i].Initialize(vortex_tmp[i].pos, vortex_tmp[i].vor);
        }
    }
};

#endif //DARTMOUTH_PHYS_COMP_VORTEXFLUID_H
