
#ifndef DARTMOUTH_PHYS_COMP_VORTEX_H
#define DARTMOUTH_PHYS_COMP_VORTEX_H

#include "Common.h"
template<int d> class Vortex{
    using VectorD=Vector<real, d>;
public:
    VectorD pos; // position, currently try to implement in 2D
    real vor; // value of vortex

    Vortex():pos(VectorD::Zero()), vor(0.){};
    Vortex(const real x_, const real y_, const real vor_):pos(VectorD(x_, y_)), vor(vor_){}
    Vortex(const real x_, const real y_, const real z_, const real vor_):pos(VectorD(x_, y_, z_)), vor(vor_){}
//    Vortex(const Vortex& copy){*this = copy;}
//    Vortex& operator=(const Vortex& copy);
    void Initialize(const VectorD& pos_=VectorD::Zero(), const real vor_=0.);

};
#endif //DARTMOUTH_PHYS_COMP_VORTEX_H
