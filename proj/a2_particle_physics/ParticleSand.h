//#####################################################################
// Particle Sand (DEM)
// Dartmouth COSC 89.18/189.02: Computational Methods for Physical Systems, Assignment starter code
// Contact: Bo Zhu (bo.zhu@dartmouth.edu)
//#####################################################################

#ifndef __ParticleSand_h__
#define __ParticleSand_h__
#include "Common.h"
#include "Particles.h"
#include "ImplicitGeometry.h"

template<int d> class ParticleSand
{using VectorD=Vector<real,d>;using VectorDi=Vector<int,d>;using MatrixD=Matrix<real,d>;
public:
	Particles<d> particles;
	real ks=(real)2e2;		////stiffness for the collision force
	real kd=(real).5e1;		////damping for the collision force
	VectorD g=VectorD::Unit(1)*(real)-1.;	////gravity

	////list of implicit geometries describing the environment, by default it has one element, a circle with its normals pointing inward (Bowl)
	Array<ImplicitGeometry<d>* > env_objects;	
	Array<Vector2i> particle_particle_collision_pairs;
	Array<Vector2i> particle_environment_collision_pairs;

	Array<VectorD> my_object_vertices={{-1.,5.},{1.,5.}};	////this array stores the positions of the contour of your object for visualization

	virtual void Advance(const real dt)
	{
		////Clear forces on particles
		for(int i=0;i<particles.Size();i++){
			particles.F(i)=VectorD::Zero();}

		////Accumulate body forces
		for(int i=0;i<particles.Size();i++){
			particles.F(i)+=particles.M(i)*g;}

		Particle_Environment_Collision_Detection();
		Particle_Environment_Collision_Response();
		Particle_Particle_Collision_Detection();
		Particle_Particle_Collision_Response();
		Particle_My_Object_Collision_Detection_And_Response();

		for(int i=0;i<particles.Size();i++){
			particles.V(i)+=particles.F(i)/particles.M(i)*dt;
			particles.X(i)+=particles.V(i)*dt;}
	}

	bool Check_Collision_Particle_Environment(int particle_idx, int env_idx) {
		const VectorD& pos = particles.X(particle_idx);
		real phi = env_objects[env_idx]->Phi(pos);
		return phi - particles.R(particle_idx) < 0;
	}

	bool Check_Collision_Particle_Particle(int i, int j) {
		VectorD c_ij = particles.X(i) - particles.X(j);
		real len = c_ij.norm();
		real r_i = particles.R(i), r_j = particles.R(j);
		return len - (r_i + r_j) < 0;
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): detect collision between particles and env_objects (implicit surface) and record the detected collisions in particle_environment_collision_pairs
	////env_objects is a list of implicit geometries, by default there is only one geometry (the bowl) in the list
	////Each element in particle_environment_collision_pairs is a Vector2i, with the first index for the particle and the second index for the env_objects
	virtual void Particle_Environment_Collision_Detection()
	{
		particle_environment_collision_pairs.clear();
		/* Your implementation start */
		for (int p_idx = 0; p_idx < particles.Size(); p_idx++) {
			for (int e_idx = 0; e_idx < env_objects.size(); e_idx++) {
				if (Check_Collision_Particle_Environment(p_idx, e_idx)) {
					particle_environment_collision_pairs.push_back(Vector2i(p_idx, e_idx));
				}
			}
		}
		/* Your implementation end */
	}
		
	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): compute the penalty-based collision force for the particles that are colliding with the env_objects
	////The collision response force consists of a spring force and a damping force
	virtual void Particle_Environment_Collision_Response()
	{
		for(int pair_idx=0;pair_idx<particle_environment_collision_pairs.size();pair_idx++){
			int i=particle_environment_collision_pairs[pair_idx][0];	////particle index
			int j=particle_environment_collision_pairs[pair_idx][1];	////env_objects index
			VectorD collision_force=VectorD::Zero();

			/* Your implementation start */
			VectorD collision_pos = particles.X(i);//an approximation...
			real r = particles.R(i);
			VectorD norm = env_objects[j]->Normal(collision_pos);
			real phi = env_objects[j]->Phi(collision_pos);
			//spring force
			collision_force += ks * (phi - r) * (-norm);
			//damping force
			VectorD vel = particles.V(i);
			real v_proj = vel.dot(norm);//it's the same as (-vel).dot(-norm)
			collision_force += kd * v_proj * (-norm);
			/* Your implementation end */
			
			particles.F(i)+=collision_force;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): find all the pairs of particles that are colliding each other and record the detected pairs in particle_particle_collision_pairs
	////Each element in particle_particle_collision_pairs is a Vector2i specifying the indices of the two colliding particles
	virtual void Particle_Particle_Collision_Detection()
	{
		particle_particle_collision_pairs.clear();
		/* Your implementation start */
		for (int i = 0; i < particles.Size(); i++) {
			for (int j = i + 1; j < particles.Size(); j++) {
				if (Check_Collision_Particle_Particle(i, j)) {
					particle_particle_collision_pairs.push_back(Vector2i(i, j));
				}
			}
		}
		/* Your implementation end */
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): compute penalty-based collision forces for pairs of colliding particles in particle_particle_collision_pairs and add the forces to particle.F 
	////The collision response force for each pair consists of a spring force and a damping force
	virtual void Particle_Particle_Collision_Response()
	{
		for(int pair_idx=0;pair_idx<particle_particle_collision_pairs.size();pair_idx++){
			int i=particle_particle_collision_pairs[pair_idx][0];	////the first particle index in the pair
			int j=particle_particle_collision_pairs[pair_idx][1];	////the second particle index in the pair
			VectorD collision_force=VectorD::Zero();

			/* Your implementation start */
			const VectorD& x_i = particles.X(i);
			const VectorD& x_j = particles.X(j);
			VectorD c_ij = x_j - x_i;
			VectorD n_ij = c_ij.normalized();
			real l_0 = particles.R(i) + particles.R(j);
			//spring force
			collision_force += ks * (c_ij.norm() - l_0) * n_ij;
			//damping force
			VectorD v_diff = particles.V(j) - particles.V(i);
			real v_proj = v_diff.dot(n_ij);
			collision_force += kd * v_proj * n_ij;
			//add force
			particles.F(i) += collision_force;
			particles.F(j) -= collision_force;
			/* Your implementation end */
		}

	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): implement your the collision detection algorithm and calculate the response forces for your own object
	////YOUR IMPLEMENTATION (P1 TASK): visualize your object by filling up the contour vertex array
	////Note: the visualization does not have to be the same as the way you represent the geometry, 
	////E.g., you may use an implicit function to calculate the geometry and the collision force, but still use the vertices to draw it
	virtual void Particle_My_Object_Collision_Detection_And_Response()
	{
		////if you want to visualize your object, fill the array of my_object_shape with its contour vertices, the commented code provides an example
		real pi = acos(-1.0);
		real r = 0.5;
		VectorD c_bowl = VectorD::Unit(1) * 8;
		VectorD c_0 = VectorD::Zero();
		VectorD c_1 = c_bowl * 2 - c_0;
		ImplicitGeometry<d>* gate = new Sphere<d>(c_0, r);

		my_object_vertices.clear();
		for (int deg = 0; deg < 360; deg++) {
			real rad = deg * pi / 180.0;
			VectorD offset = VectorD::Zero();
			offset(0) = cos(rad) * r, offset(1) = sin(rad) * r;
			if (deg < 180) {//lower half
				my_object_vertices.push_back(c_0 + offset);
			}
			else {//upper half
				my_object_vertices.push_back(c_1 + offset);
			}
		}
		
		VectorD v_0 = VectorD::Zero();
		real rate = 10.0, rad = 0;
		for(int i=0;i<particles.Size();i++){
			const VectorD& pos = particles.X(i);
			if (gate->Phi(pos) - particles.R(i) < 0) {
				particles.X(i) = 2 * c_bowl - pos;
				real rad = rand() % 19 * (-10) * pi / 180.0;
				v_0(0) = rate * cos(rad), v_0(1) = rate * sin(rad);
				particles.V(i) = v_0;
				particles.F(i) = VectorD::Zero();
			}
		}	
	}
};

#endif
