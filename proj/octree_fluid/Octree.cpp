//////////////////////////////////////////////////////////////////////////
// Octree
// Copyright (c) (2018-), Mengdi Wang, Dongkai Chen
// This file is part of SimpleX, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#include "Octree.h"

template<int d>
int Octree<d>::Child_Index(const VectorD& pos)
{
    //an up to 3-digit binary number: zyx
    //0 for lower, 1 for higher
    int index = 0;
    for (int axis = 0; axis < d; axis++) {
        index <<= 1;
        index |= (pos[axis] <= center[axis] ? 0 : 1);
    }
    return index;
}

template<int d>
void Octree<d>::insert(VectorD point)
{
    /*
    // check if the point is inside the box
    for (int i = 0; i < d; i++) {
        if (point[i] > box.max_corner[i] || point[i] < box.min_corner[i]) {
            return;
        }
    }
    int pos = get_cube_pos(point, center);

    // check if is an region node for further inserting
    if (!(children[pos]->is_empty_node || children[pos]->is_leaf)) {
        children[pos]->insert(point);
        return;
    }
    else if (children[pos]->is_empty_node) {
        children[pos] = std::make_shared<Octree<d>>();
        return;
    }
    else {
        VectorD tmp_pos = children[pos]->center;
        children[pos] = nullptr;
        if (pos == LTF) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == RTF) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == RBF) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == LBF) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == LTB) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == RTB) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == RBB) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        else if (pos == LBB) {
            children[pos] = std::make_shared<Octree<d>>();
        }
        children[pos]->insert(tmp_pos);
        children[pos]->insert(point);
    }
    return;*/
}
template<int d>
bool Octree<d>::find(VectorD point)
{
    /*for (int i = 0; i < d; i++) {
        if (point[i] > box.max_corner[i] || point[i] < box.min_corner[i]) {
            return false;
        }
    }
    //int pos = get_cube_pos(point, center);

    if (!(children[pos]->is_empty_node || children[pos]->is_leaf)) {
        return children[pos]->find(point);
    }
    else if (children[pos]->is_empty_node) {
        return false;
    }
    else {
        if (point == children[pos]->center) {
            return true;
        }
    }*/
    return false;
}
template class CornerNode<2>;
template class CornerNode<3>;
template class Octree<2>;
template class Octree<3>;