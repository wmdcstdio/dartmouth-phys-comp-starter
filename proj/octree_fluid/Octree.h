//////////////////////////////////////////////////////////////////////////
// Octree
// Copyright (c) (2018-), Mengdi Wang, Dongkai Chen
// This file is part of SimpleX, whose distribution is governed by the LICENSE file.
//////////////////////////////////////////////////////////////////////////
#ifndef __Octree_h__
#define __Octree_h__
#include "Common.h"

#define LTF 0    // left top front
#define RTF 1    // right top front
#define RBF 2    // right bottom front
#define LBF 3    // left bottom front
#define LTB 4    // left top back
#define RTB 5    // right top back
#define RBB 6    // right bottom back
#define LBB 7    // left bottom back

class FaceNode {
public:
	real vel;
};

class CenterNode {
public:
	real p;
};

template<int d>
class CornerNode {
	using VectorD = Vector<real, d>;
public:
	int coarsest_leaf_level;
	VectorD vel;
	Array<std::shared_ptr<Octree<d>>>  adj_cells;
};

template<int d>
class Octree {
	using VectorD = Vector<real, d>;
public:
	int level = 0;
	bool is_leaf = false;
	bool is_empty_node = false;
	Box<d> box; // bottom right point & top left point ?
	VectorD center; //center position?

    Array<std::shared_ptr<Octree<d>>> children; //when !is_leaf, it's an array of 8 "Octree<d>*"s.
    Array<std::shared_ptr<Octree<d>>> neighbors;//when !is_leaf, its length is 6
    CenterNode center_val;
    Array<std::shared_ptr<FaceNode>> face_nodes;//when !is_leaf, its size is 6. using shared_ptr because values are hold here.
    Array<std::shared_ptr<CornerNode<d>>> corner_nodes;//always there

	Octree(){}// todo add arguments
	~Octree(){}

	int Axis_Digit(int index, int axis) { return (index >> axis) & 1; }
    const static int Child_Size(void) { return (d == 2 ? 4 : 8); }
    const static int Neighbor_Size(void) { return (d == 2 ? 4 : 6); }

    int Child_Index(const VectorD &pos);
	template<class T> T Intp_Value(const VectorD& pos, std::function<T(std::shared_ptr<CornerNode<d>>)>& f);

	void Clear_Corner_Coarest_Leaf_Levels(void);

    void insert(VectorD point);
    bool find(VectorD point);
};

template<int d>
template<class T>
inline T Octree<d>::Intp_Value(const VectorD& pos, std::function<T(std::shared_ptr<CornerNode<d>>)>& f)
{
	if (!is_leaf) {
		return children[Child_Index(pos)]->Intp_Value(pos, f);
	}
	VectorD frac, len = box.Edge_Lengths();
	for (int axis = 0; axis < d; axis++) frac[i] = (pos[i] - box.min_corner[i]) / len[i];
	int n = Child_Size();
	T ret;
	for (int idx = 0; idx < n; idx++) {
		real w = 1.0;
		for (int axis = 0; axis < d; axis++) {
			int k = Axis_Digit(idx, axis);
			w *= (k == 1 ? frac[axis] : 1.0 - frac[axis]);
		}
		T tmp = w * f(corner_nodes[idx]);
		if (idx == 0) ret = tmp;//ugly... but we don't know how to assign ret=0
		else ret += tmp;
	}
	return ret;
}



#endif

